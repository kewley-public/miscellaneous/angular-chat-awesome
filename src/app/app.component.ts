import { Component } from '@angular/core';
import * as momentNS from 'moment-timezone';
import { ChatMessageType } from '../../projects/angular-chat-awesome/src/lib/models/chat-message-type.model';
import { ChatMessage } from '../../projects/angular-chat-awesome/src/lib/models/chat-message.model';

const moment = momentNS;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  timezone = 'America/Chicago';
  title = 'Angular Chat Awesome';
  chatMessages: ChatMessage[] = [
    {
      type: ChatMessageType.RECEIVED,
      message: 'Hello there!',
      displayName: 'Dan',
      displayUrl: 'assets/demo_icon_one.png',
      timeStampMillis: moment.tz(this.timezone).subtract(1, 'day').valueOf(),
    },
    {
      type: ChatMessageType.SENT,
      message: 'Hiya',
      displayName: 'Mark',
      displayUrl: 'assets/demo_icon_two.png',
      timeStampMillis: moment.tz(this.timezone).valueOf(),
    },
  ];

  private _newMessage: string;

  public get newMessage() {
    return this._newMessage;
  }

  public set newMessage(message: string) {
    this._newMessage = message;
  }

  onSubmit() {
    this.chatMessages = [
      ...this.chatMessages,
      {
        type: ChatMessageType.SENT,
        message: this._newMessage,
        displayName: 'Mark',
        displayUrl: 'assets/demo_icon_two.png',
        timeStampMillis: moment.tz(this.timezone).valueOf(),
      },
    ];
    this.newMessage = null;
  }

}
