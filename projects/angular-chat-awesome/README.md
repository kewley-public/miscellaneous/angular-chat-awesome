# Angular Chat Awesome

Basic chat window for angular using an angular inspired [quill-editor](https://quilljs.com/)

## Demo

Demo UI is hosted on AWS CloudFront which can be found [here](http://d2mvk4oe8oeoo7.cloudfront.net)

## TODO
1. Mobile look and feel

## Peer Dependencies 

```json
  "peerDependencies": {
    "@angular/common": "^6.0.0-rc.0 || ^6.0.0",
    "@angular/core": "^6.0.0-rc.0 || ^6.0.0",
    "bootstrap": "^4.1.3",
    "moment-timezone": "^0.5.21",
    "ngx-quill": "^3.4.0"
  }
```
 
## Installation

```bash 
npm install --save bootstrap moment-timezone ngx-quill angular-chat-awesome
```

## Usage

Please use with Angular 6 only

### App module

```javascript
@NgModule({
    ...
    imports: [
        ...,
        AngularChatAwesomeModule,
        ...
    ],
    ...
})
export class AppModule {
}
```

### Chat Message/Chat Message Type

The chatMessages follows the following models:

```typescript
enum ChatMessageType {
  SENT,
  RECEIVED
}

interface ChatMessage {
  type: ChatMessageType;
  message: string;
  timeStampMillis: number;
  displayName?: string; // used in place of the image url
  displayUrl?: string; // image badge next to message
}

```

### Template
```angular2html
      <lib-angular-chat-awesome
        [contactName]="'Joe Bagley'"
        [messages]="chatMessages"
        [timezone]="timezone"
        [height]="heightValue"
        [maxHeight]="maxHeightValue"
        [(newMessage)]="newMessage">
      </lib-angular-chat-awesome>
```

## Properties
| Property | Description | Default Value
| :---     |     :---   | :---
| contactName | The name of the person you are messaging with | null
| messages | A collection of `ChatMessage`s to display | []
| timezone | The timezone used for message timestamps | UTC
| height | A string value representing height of the chat modal (e.g. 200px) | `'100%'`
| maxHeight | A string value representing max height of the chat modal (e.g. 200px) | `'100%'`
| newMessage | Two way binding to the input HTML from the quill-editor | null


