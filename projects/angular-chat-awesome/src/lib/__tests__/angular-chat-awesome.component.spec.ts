import { AngularChatAwesomeComponent } from '../angular-chat-awesome.component';
import { FormBuilder } from '@angular/forms';
import { ChatMessageType } from '../models/chat-message-type.model';
import { ChatMessage } from '../models/chat-message.model';
import { SimpleChange, SimpleChanges } from '@angular/core';

describe('AngularChatAwesomeComponent', () => {
  let component: AngularChatAwesomeComponent;

  const setupComponent = () => {
    component = new AngularChatAwesomeComponent(new FormBuilder());
    jest.spyOn(component.newMessageChange, 'emit');
  };

  beforeEach(() => setupComponent());

  describe('Sanity Checks', () => {
    it('Should create', () => {
      expect(component).toBeTruthy();
    });

    it('Should have the expected default values', () => {
      const {form} = component;
      expect(form.value).toEqual({message: null});
    });
  });

  describe('ngOnChanges', () => {
    [
      {
        message: 'do nothing when the changes do not contain newMessage',
        changes: {} as SimpleChanges,
        originalFormValue: {message: `I've got a lovely bunch of coconuts!`} as any,
        expectedFormValue: {message: `I've got a lovely bunch of coconuts!`} as any,
      },
      {
        message: 'do nothing when the changes do contain newMessage with a non-null value',
        changes: {newMessage: {currentValue: 'Blah'} as SimpleChange} as SimpleChanges,
        originalFormValue: {message: `I've got a lovely bunch of coconuts!`} as any,
        expectedFormValue: {message: `I've got a lovely bunch of coconuts!`} as any,
      },
      {
        message: 'clear the new message when the changes contain a newMessage of null',
        changes: {newMessage: {currentValue: null} as SimpleChange} as SimpleChanges,
        originalFormValue: {message: `I've got a lovely bunch of coconuts!`} as any,
        expectedFormValue: {message: null} as any,
      },
      {
        message: 'clear the new message when the changes contain a newMessage of null',
        changes: {newMessage: {currentValue: undefined} as SimpleChange} as SimpleChanges,
        originalFormValue: {message: `I've got a lovely bunch of coconuts!`} as any,
        expectedFormValue: {message: null} as any,
      },
    ].forEach(({message, changes, originalFormValue, expectedFormValue}) => {
      it(`Should ${message}`, () => {
        component.form.setValue(originalFormValue);
        component.ngOnChanges(changes);

        expect(component.form.value).toEqual(expectedFormValue);
      });
    });
  });

  describe('determineMessageClass', () => {
    [
      {
        type: undefined as ChatMessageType,
        expected: '',
      },
      {
        type: null as ChatMessageType,
        expected: '',
      },
      {
        type: ChatMessageType.SENT,
        expected: 'sent',
      },
      {
        type: ChatMessageType.RECEIVED,
        expected: 'received',
      },
    ].forEach(({type, expected}) => {
      it(`Should return ${expected} for message type ${type}`, () => {
        expect(component.determineMessageClass({type} as ChatMessage))
          .toEqual(expected);
      });
    });
  });

  describe('onContentChange', () => {
    it('Should update the state and dispatch an event', () => {
      const html = `I've got a lovely bunch of coconuts!`;
      component.onContentChange({html});
      expect(component.newMessage).toEqual(html);
      expect(component.newMessageChange.emit).toHaveBeenCalledTimes(1);
    });
  });

  describe('determineFlexClass', () => {
    [
      {
        type: undefined as ChatMessageType,
        expected: '',
      },
      {
        type: null as ChatMessageType,
        expected: '',
      },
      {
        type: ChatMessageType.SENT,
        expected: 'justify-content-start',
      },
      {
        type: ChatMessageType.RECEIVED,
        expected: 'flex-row-reverse',
      },
    ].forEach(({type, expected}) => {
      it(`Should return ${expected} for message type ${type}`, () => {
        expect(component.determineFlexClass({type} as ChatMessage))
          .toEqual(expected);
      });
    });
  });

});
