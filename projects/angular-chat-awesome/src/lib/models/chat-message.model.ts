import { ChatMessageType } from './chat-message-type.model';

export interface ChatMessage {
  type: ChatMessageType;
  message: string;
  timeStampMillis: number;
  displayName?: string;
  displayUrl?: string;
}
