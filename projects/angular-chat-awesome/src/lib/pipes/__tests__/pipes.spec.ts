import * as momentNS from 'moment-timezone';
import { DateTimeFromMillisPipe } from '../pipes';

const moment = momentNS;

describe('Pipes', () => {
  describe('DateTimeFromMillisPipe', () => {
    let pipe: DateTimeFromMillisPipe;

    const setupPipe = () => pipe = new DateTimeFromMillisPipe();

    const mockDateDeterminations = (timezone: string) => {
      // @ts-ignore ignoring restrictions for test
      jest.spyOn(pipe, 'determineBeginningOfTomorrow').mockReturnValueOnce(moment.tz('2018-01-02', timezone).startOf('day'));
      // @ts-ignore ignoring restrictions for test
      jest.spyOn(pipe, 'determineBeginningOfToday').mockReturnValueOnce(moment.tz('2018-01-01', timezone).startOf('day'));
    };

    beforeEach(() => setupPipe());

    describe('transform', () => {
      [
        {
          value: moment.tz('2018-01-01 09:00', 'UTC').valueOf(),
          timezone: 'UTC',
          expected: '9:00 AM',
        },
        {
          value: moment.tz('2018-01-01 09:00', 'UTC').valueOf(),
          timezone: 'America/Chicago',
          expected: '3:00 AM',
        },
        {
          value: moment.tz('2017-12-25 09:00', 'UTC').valueOf(),
          timezone: 'UTC',
          expected: 'Monday December 25, 2017 9:00 AM',
        },
      ].forEach(({value, timezone, expected}) => {
        it(`Should transform ${value} to ${expected}`, () => {
          mockDateDeterminations(timezone);

          const pipeTransformResult = pipe.transform(value, timezone);
          expect(pipeTransformResult)
            .toEqual(expected);
        });
      });

      describe('determineFormat', () => {
        [
          {
            value: moment.tz('2018-01-01 09:00', 'UTC').valueOf(),
            expected: 'h:mm A',
          },
          {
            value: moment.tz('2018-12-31 09:00', 'UTC').valueOf(),
            expected: 'dddd MMMM DD, YYYY h:mm A',
          },
        ].forEach(({expected, value}) => {
          it(`Should return ${expected} for ${value}`, () => {
            mockDateDeterminations('UTC');

            // @ts-ignore ignoring restrictions for test
            expect(pipe.determineFormat(value, 'UTC'))
              .toEqual(expected);
          });
        });
      });

      describe('determineIfValueFallsWithinCurrentDay', () => {
        [
          {
            value: moment.tz('2018-01-01 09:00', 'UTC').valueOf(),
            expected: true,
          },
          {
            value: moment.tz('2018-12-31 09:00', 'UTC').valueOf(),
            expected: false,
          },
          {
            value: moment.tz('2018-01-02 09:00', 'UTC').valueOf(),
            expected: false,
          },
        ].forEach(({value, expected}) => {
          it(`Shouled be ${expected} when value is ${value}`, () => {
            mockDateDeterminations('UTC');

            // @ts-ignore ignoring restrictions for test
            expect(pipe.determineIfValueFallsWithinCurrentDay(value, 'UTC'))
              .toEqual(expected);
          });
        });
      });

      describe('determineBeginningOfTomorrow', () => {
        it('Should determine the beginning of tomorrows date', () => {
          // @ts-ignore ignoring restrictions for test
          expect(pipe.determineBeginningOfTomorrow('UTC')).toEqual(
            moment.tz('UTC')
              .add(1, 'day')
              .startOf('day')
              .valueOf(),
          );
        });
      });

      describe('determineBeginningOfToday', () => {
        it('Should determine the beginning of todays date', () => {
          // @ts-ignore ignoring restrictions for test
          expect(pipe.determineBeginningOfToday('UTC')).toEqual(
            moment.tz('UTC')
              .startOf('day')
              .valueOf(),
          );
        });
      });

    });
  });
});
