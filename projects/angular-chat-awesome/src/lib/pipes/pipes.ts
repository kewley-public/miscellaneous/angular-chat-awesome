import { Pipe, PipeTransform } from '@angular/core';
import * as momentNS from 'moment-timezone';

const moment = momentNS;

@Pipe({name: 'dateTimeFromMillis'})
export class DateTimeFromMillisPipe implements PipeTransform {
  transform(value: number, timezone: string): string {
    if (value === undefined || value === null) {
      return ('');
    }

    const format = this.determineFormat(value, timezone);
    if (timezone) {
      return moment.tz(value, timezone).format(format);
    } else {
      return moment.tz(value).format(format);
    }
  }

  private determineFormat(value: number, timezone: string): string {
    if (this.determineIfValueFallsWithinCurrentDay(value, timezone)) {
      return 'h:mm A';
    }
    return 'dddd MMMM DD, YYYY h:mm A';
  }

  private determineIfValueFallsWithinCurrentDay(value: number, timezone: string): boolean {
    const tomorrow = this.determineBeginningOfTomorrow(timezone);
    const beginningOfToday = this.determineBeginningOfToday(timezone);
    return beginningOfToday <= value && value <= tomorrow;
  }

  private determineBeginningOfTomorrow(timezone?: string): number {
    const momentValue = timezone ? moment.tz(timezone) : moment();
    return momentValue
      .add(1, 'day')
      .startOf('day')
      .valueOf();
  }

  private determineBeginningOfToday(timezone?: string): number {
    const momentValue = timezone ? moment.tz(timezone) : moment();
    return momentValue
      .startOf('day')
      .valueOf();
  }
}
